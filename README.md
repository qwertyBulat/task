package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        String path = "E:\\Task\\Task.txt";

        File file = new File(path);

        Scanner scanner = new Scanner(file);

        String line = scanner.nextLine();
        String[] sNumbers = line.split(",");

        Integer[] numbers = new Integer[sNumbers.length];
        int counter = 0;


        for (String number : sNumbers) {
            numbers[counter++] = Integer.parseInt(number);
        }

        //Arrays.sort(numbers);  // по возрастанию
        //Arrays.sort(numbers, Collections.reverseOrder()); // по убыванию

        System.out.println(Arrays.toString(numbers));

        scanner.close();
    }
}
